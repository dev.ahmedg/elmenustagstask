package com.elmenus.base.di

import com.elmenus.base.utils.FileManager
import com.elmenus.base.utils.NetworkHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/**
 * Koin utils module
 */
val utilsModule = module {
    single {
        FileManager(androidApplication())
    }

    single { NetworkHelper() }
}
