package com.elmenus.base.baseEntities

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


/**
 * Base activity
 *
 * @param DB the data binding class.
 */
abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity(){




    @LayoutRes
    abstract fun getLayoutRes(): Int


    lateinit var mBinding : DB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding =  DataBindingUtil.setContentView(this, getLayoutRes()) as DB

        onViewAttach()

    }



    override fun onStart() {
        super.onStart()
        onViewRefresh()
    }

    //OnStart
    open fun onViewRefresh() {

    }

    //OnCreate
    open fun onViewAttach() {

    }


}