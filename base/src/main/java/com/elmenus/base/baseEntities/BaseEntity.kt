package com.elmenus.base.baseEntities

/**
 * base data item class that are used in PagedListAdapter for the Diff callback id comparison
 */
interface  BaseEntity {

    fun entityId():String
}