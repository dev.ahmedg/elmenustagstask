package com.elmenus.base.baseEntities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.elmenus.base.R
import com.gturedi.views.StatefulLayout


/**
 * Base fragment
 *
 * @param DB the data binding class.
 */
abstract class BaseFragment<DB : ViewDataBinding>() : Fragment() {



    lateinit var mBinding: DB

    fun init(inflater: LayoutInflater, container: ViewGroup) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    abstract fun initViewModel()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        init(inflater, container!!)
        super.onCreateView(inflater, container, savedInstanceState)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        onViewAttach()
    }


    override fun onResume() {
        super.onResume()
        onViewRefresh()
    }


    open fun onViewRefresh() {

    }


    open fun onViewAttach() {

    }

    fun showLoading(stateFullLayout:StatefulLayout) {
        stateFullLayout.showLoading()
    }

    fun showEmpty(stateFullLayout:StatefulLayout) {
        stateFullLayout.showEmpty()
    }

    fun showError(stateFullLayout:StatefulLayout,error:String?  , retry :()->Unit) {
        stateFullLayout.showError(error?:getString(R.string.something_went_wrong)){retry()}
    }

    fun showContent(stateFullLayout:StatefulLayout) {
        stateFullLayout.showContent()
    }



}