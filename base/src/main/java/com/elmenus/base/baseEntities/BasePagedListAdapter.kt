package com.elmenus.base.baseEntities

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.elmenus.base.R


/**
 * Base pagination list adapter
 *
 * Utilize creating and binding data into viewHolder
 * @see BaseEntity
 * @see BaseViewHolder
 */
abstract class BasePagedListAdapter(diffCallback: DiffUtil.ItemCallback<BaseEntity> = BaseDiffCallback())
    : PagingDataAdapter<BaseEntity, RecyclerView.ViewHolder>(diffCallback) {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return getViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as BaseViewHolder<*>).binding.root.setTag(R.string.position, position)

        bind(holder.binding, position)

    }


    open fun getViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        BaseViewHolder(createBinding(parent, viewType))


    abstract fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding


    protected abstract fun bind(binding: ViewDataBinding, position: Int)


    suspend fun setList(list: PagingData<*>) {
        submitData(list as PagingData<BaseEntity>)
    }



    companion object {
        open class BaseDiffCallback : DiffUtil.ItemCallback<BaseEntity>() {
            override fun areItemsTheSame(oldItem: BaseEntity, newItem: BaseEntity) = oldItem.entityId() == newItem.entityId()


            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: BaseEntity, newItem: BaseEntity) = oldItem == newItem
        }
    }
}