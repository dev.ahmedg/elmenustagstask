# Elmenus
This repository is based on a modularized MVVM and clean architectures with the usage of Use cases pattern which has the following benifits :
 1. Avoiding god objects (viewModels) that deals with both presentation logic and dataflow logic.
 2. Reusing of dataflow logic across different ViewModels.

## Modules
 1. Base module : contains superclasses for the common components (activity , fragment ,...)
 2. Data module : contains both persistance and network layers , repositories are here to be sharable cross all features
 3. Features modules : contain view , view models and usecase (Menus - item details)
 4. app module : contains the host Activity for navigation support an the App class
 
 ## Stack
 1. Kotlin.
 2. Coroutines with flow.
 3. Live data.
 4. Navigation component.
 5. MVVM with clean arch (Abstract among layers) and use cases
 6. Room database
 7. Robot pattern for ui test
 
 ## Ui tests
 Espresso is used with some other libs that enable navigation hosted fragments tests , ui tests work idea is to mock api responses in different cases 
 and ensure that views behave as supposed , i have used followong tools in ui test
 1. Espresso 
 2. Junit4
 3. RESTMock lib - to mock server responses for ui tests.
 4. Mockito to replace real dependencies with mocks ones in test

 ## Unit and instrumentation tests
 Mainly mockito and other mocking libs are used toi provide fake depencies .

 1. Junit4 
 2. Junit Asserttions
 4. Mockito.
 5. kluent.
 6. Dex opener.
 
 ## Top tools
 1. Paging 3 lib with mediator usage to handle caching. 
 2. Koin - DI - used for locating dependencies cross all app including ui test.
 3. RESTMock - to mock server responses for ui test.
 4. SDP library to support large screes in a simple way.
 5. Mockito
 5. Espresso
 
### The source code is partially documented
