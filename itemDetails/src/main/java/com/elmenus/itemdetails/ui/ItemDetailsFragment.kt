package com.elmenus.itemdetails.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.elmenus.base.baseEntities.BaseFragment
import com.elmenus.data.network.entities.Item
import com.elmenus.itemdetails.R
import com.elmenus.itemdetails.databinding.FragmentItemDetailsBinding

/**
 * Item details fragment
 *
 * Shows item image , title , description
 * @see Item
 */
class ItemDetailsFragment : BaseFragment<FragmentItemDetailsBinding>() {


    private val item by lazy { arguments?.getParcelable<Item>(KEY_ITEM) }

    override fun getLayoutRes() = R.layout.fragment_item_details

    override fun initViewModel() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSharedElementTransitionOnEnter()
    }

    override fun onViewAttach() {
        super.onViewAttach()
        setUpToolbar()
        postponeEnterTransition()

        populateTxtViews()

        mBinding.ivItem.apply {
            transitionName = item?.photoUrl
            startEnterTransitionAfterLoadingImage(item!!.photoUrl,this)
        }

    }

    private fun startEnterTransitionAfterLoadingImage(imageAddress: String, imageView: ImageView) {
        Glide.with(this)
            .load(imageAddress)
            .dontAnimate()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }
            })
            .into(imageView)
    }

    private fun populateTxtViews() {
        mBinding.tvDescription.text = item?.description
    }

    private fun setUpToolbar() {
        val activity = (requireActivity() as? AppCompatActivity)
        activity?.setSupportActionBar(mBinding.toolbar)
        activity?.supportActionBar?.title = item?.name
        activity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        const val KEY_ITEM = "item"
        fun buildArgs(item: Item) = Bundle().apply { putParcelable(KEY_ITEM,item) }
    }

    private fun setSharedElementTransitionOnEnter() {
        sharedElementEnterTransition = TransitionInflater.from(context)
            .inflateTransition(android.R.transition.move)
    }




}