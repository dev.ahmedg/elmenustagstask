package com.elmenus.tags.base.rules

import com.elmenus.tags.base.utils.RestMockSetting
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement


/**
 * Rest mock library rule
 *
 * Reset api mocks before and after each ui test case
 *
 */
class RestMockRule: TestRule  {


    override fun apply(base: Statement, description: Description): Statement {
        return object: Statement() {
            override fun evaluate() {

                with(RestMockSetting){
                    clearMocks()
                    startMockedApisListener()
                    base.evaluate()
                    stopMockedApisListener()
                    clearMocks()
                }

            }
        }
    }
}