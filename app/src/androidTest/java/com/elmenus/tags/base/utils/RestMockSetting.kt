package com.elmenus.tags.base.utils



import com.elmenus.data.BuildConfig
import com.elmenus.data.constant.BASE_API_URL
import com.elmenus.data.di.networkModule
import io.appflate.restmock.RESTMockServer
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

/**
 * Helper for rest mock library
 */
object RestMockSetting  {


    fun startMockedApisListener() {

        val mockUrl = RESTMockServer.getUrl()

        if (BASE_API_URL == mockUrl) return

        BASE_API_URL = mockUrl

        refreshNetworkKoinModule()
    }


    fun stopMockedApisListener() {

        val realUrl = BuildConfig.BASE_API_URL

        if (BASE_API_URL == realUrl) return

        BASE_API_URL = realUrl

        refreshNetworkKoinModule()
    }


    fun clearMocks() {
        RESTMockServer.reset()
    }

    private fun refreshNetworkKoinModule(){
        unloadKoinModules(networkModule)
        loadKoinModules(networkModule)

    }

}