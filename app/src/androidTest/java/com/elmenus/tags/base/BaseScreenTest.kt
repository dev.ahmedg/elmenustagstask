package com.elmenus.tags.base

import com.elmenus.tags.base.rules.RestMockRule
import org.junit.After
import org.junit.Before
import org.junit.Rule


/**
 * Base class for screen ui tests
 */
open class BaseScreenTest  {



    @get:Rule
    val restMockRule = RestMockRule()


    @Before
    fun setUp(){
        beforeEachTest()
    }


    open fun beforeEachTest(){}


    @After
    fun tearDown(){
        afterEachTest()
    }

    open fun afterEachTest(){}


}