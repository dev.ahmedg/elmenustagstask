package com.elmenus.tags.base


import android.content.Context
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import com.elmenus.base.utils.NetworkHelper
import junit.framework.Assert.assertTrue
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.mock.declare
import org.mockito.Mockito
import java.util.concurrent.TimeUnit



fun base(func: BaseTestRobot.() -> Unit) = BaseTestRobot().apply { func() }


/**
 * Base class for robot pattern
 *
 * Has some helper methods
 */
open class BaseTestRobot : KoinTest {


    fun getString(@StringRes id: Int): String = getTargetContext().getString(id)


    fun clickView(viewId: Int): ViewInteraction = onView((withId(viewId))).perform(click())


    fun viewWithId(id: Int): ViewInteraction = onView(withId(id))

    fun matchText(viewInteraction: ViewInteraction, text: String)= viewInteraction
            .check(matches(withText(text)))

    fun matchText(@IdRes resId: Int, text: String): ViewInteraction = matchText(viewWithId(resId), text)


    fun checkViewsVisible(vararg viewsIds: Int) {
        viewsIds.forEach {
            onView(withId(it))
                    .check(matches(isDisplayed()))
        }
    }

    fun checkRecyclerViewHasData(@IdRes rvId:Int) {
        viewWithId(rvId).check { view, noViewFoundException ->
            noViewFoundException?.apply {
                throw this
            }
            assertTrue(view is RecyclerView && view.adapter !=null && view.adapter?.itemCount?:-1 > 0)
        }
    }


    fun sleep(sec: Long) = apply {
        Thread.sleep(TimeUnit.SECONDS.toMillis(sec))
    }

    fun sleepForEver() = apply {
        sleep(Long.MAX_VALUE)
    }


    inline fun <reified T:Any>mockKoinMember(applyMock: (T) -> Unit) {

        val member = get<T>()
        val mock = Mockito.spy(member)

        applyMock(mock)

        declare {
            mock
        }

    }

    fun getTargetContext(): Context = InstrumentationRegistry.getInstrumentation().targetContext


    fun getTestContext(): Context = InstrumentationRegistry.getInstrumentation().context



    fun clickRecyclerViewItem(@IdRes rvId: Int, position: Int) {
        onView(withId(rvId))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))
    }



    fun mockNetwork(connected:Boolean) {

        mockKoinMember<NetworkHelper> {
            Mockito.`when`(it.isConnected()).thenReturn(connected)
        }

    }


}