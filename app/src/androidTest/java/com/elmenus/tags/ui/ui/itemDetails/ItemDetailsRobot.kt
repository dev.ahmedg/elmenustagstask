package com.elmenus.tags.ui.ui.itemDetails

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import com.elmenus.data.network.entities.Item
import com.elmenus.itemdetails.ui.ItemDetailsFragment
import com.elmenus.tags.R
import com.elmenus.tags.base.BaseTestRobot


fun itemDetails(func: ItemDetailsRobot.() -> Unit) = ItemDetailsRobot().apply { func() }

class ItemDetailsRobot : BaseTestRobot() {

    private lateinit var scenario: FragmentScenario<ItemDetailsFragment>

    private val testItemObj = Item("","Title","Description",0)

    fun openScreen() {
        val fragmentArgs = bundleOf(ItemDetailsFragment.KEY_ITEM to testItemObj)
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_AppTheme,fragmentArgs = fragmentArgs)
    }


    fun checkTextViewDataIsCorrect() {
        matchText(viewWithId(R.id.tv_description),testItemObj.description)
    }

}