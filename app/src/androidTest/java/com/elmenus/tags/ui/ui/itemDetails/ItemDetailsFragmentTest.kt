package com.elmenus.tags.ui.ui.itemDetails

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.elmenus.tags.base.BaseScreenTest
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Ui test class for the item details cases
 */
@RunWith(AndroidJUnit4::class)
class ItemDetailsFragmentTest : BaseScreenTest(){



    @Test
    fun data_ShouldBeDisplayedCorrectlyWhenScreenIsOpenedWithData() {
        itemDetails {
            openScreen()
            checkTextViewDataIsCorrect()
        }
    }


}