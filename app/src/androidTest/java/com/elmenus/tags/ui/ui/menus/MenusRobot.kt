package com.elmenus.tags.ui.ui.menus

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers
import com.elmenus.data.constant.ITEMS_LIST_API
import com.elmenus.data.constant.TAGS_LIST_API
import com.elmenus.data.database.AppDatabase
import com.elmenus.menus.ui.MenusFragment
import com.elmenus.tags.R
import com.elmenus.tags.base.BaseTestRobot
import com.google.common.truth.Truth.assertThat
import io.appflate.restmock.RESTMockServer
import io.appflate.restmock.utils.RequestMatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.hamcrest.Matchers
import org.koin.core.component.get


fun menus(func: MenusRobot.() -> Unit) = MenusRobot().apply { func() }

class MenusRobot : BaseTestRobot() {

    private lateinit var scenario: FragmentScenario<MenusFragment>



    fun openScreen() {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_AppTheme)
    }

    fun clearDatabase() {
        val db = get<AppDatabase>()
        GlobalScope.launch {
            db.tagsDao().clearAll()
            db.tagRemoteKeyDao().clearRemoteKeys()
            getTargetContext().filesDir.deleteRecursively()
        }
    }

    fun mockTagsWithData() {
        RESTMockServer.whenGET(RequestMatchers.pathContains(TAGS_LIST_API))
            .thenReturnFile("tag_list_page.json")
    }

    fun mockItemsListWithData() {
        RESTMockServer.whenGET(RequestMatchers.pathContains(ITEMS_LIST_API))
            .thenReturnFile("items_list.json")
    }

    fun mockItemsListWithEmpty() {
        RESTMockServer.whenGET(RequestMatchers.pathContains(ITEMS_LIST_API))
            .thenReturnFile("items_list_empty.json")
    }

    fun clickTagItem() {
        clickRecyclerViewItem(R.id.rv_tags,0)
    }

    fun clickMenusItem() {
        clickRecyclerViewItem(R.id.rv_items,0)
    }

    fun sleepForSharedPostpone(){
        sleep(5)
    }

    fun checkItemDetailsOpened() {
        // Create a TestNavHostController
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        scenario.onFragment { fragment ->
            // Set the graph on the TestNavHostController
            navController.setGraph(R.navigation.navigation_menus)

            // Make the NavController available via the findNavController() APIs
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
        clickMenusItem()
        assertThat(navController.currentDestination?.id).isEqualTo(R.id.itemDetailsFragment)
    }

    fun checkTagsListHasData() {
        checkRecyclerViewHasData(R.id.rv_tags)
    }

    fun checkItemsListHasData() {
        checkRecyclerViewHasData(R.id.rv_items)
    }

    fun checkItemsEmptyViewVisible() {
        matchText(
            Espresso.onView(
                Matchers.allOf(
                    ViewMatchers.withId(R.id.stMessage),
                    ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.stateful_items))
                )
            ),getString(R.string.stfEmptyMessage))
    }

    fun checkItemsNetworkErrorViewVisible() {
        matchText(
            Espresso.onView(
                Matchers.allOf(
                    ViewMatchers.withId(R.id.stMessage),
                    ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.stateful_items))
                )
            ),getString(R.string.no_internet_connection))
    }


    fun checkTagsNetworkErrorViewVisible() {
        matchText(
            Espresso.onView(
                Matchers.allOf(
                    ViewMatchers.withId(R.id.stMessage),
                    ViewMatchers.isDescendantOfA(ViewMatchers.withId(R.id.stateful_tags))
                )
            ),getString(R.string.no_internet_connection))
    }

}