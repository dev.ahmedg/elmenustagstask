package com.elmenus.tags.ui.ui.menus

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.elmenus.tags.base.BaseScreenTest
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Ui test class for the menus screen cases
 */
@RunWith(AndroidJUnit4::class)
class MenusFragmentTest : BaseScreenTest(){



    @Test
    fun emptyView_shouldAppearWhenNoItemsIsAvailable() {
        menus {
            clearDatabase()
            mockTagsWithData()
            mockItemsListWithEmpty()
            openScreen()
            sleepForSharedPostpone()
            clickTagItem()
            checkItemsEmptyViewVisible()
        }
    }

    @Test
    fun errorView_shouldAppearWhenNetworkIsNotAvailable() {
        menus {
            clearDatabase()
            mockTagsWithData()
            mockNetwork(false)
            openScreen()
            sleepForSharedPostpone()
            checkTagsNetworkErrorViewVisible()
        }
    }


    @Test
    fun recyclerViews_shouldContainDataWhenDataAvailable() {
        menus {
            clearDatabase()
            mockTagsWithData()
            mockItemsListWithData()
            openScreen()
            sleepForSharedPostpone()
            clickTagItem()
            checkItemsListHasData()
        }
    }

    @Test
    fun itemDetails_shouldBeOpenedWhenClickOnItem() {
        menus {
            clearDatabase()
            mockTagsWithData()
            mockItemsListWithData()
            openScreen()
            sleepForSharedPostpone()
            clickTagItem()
            checkItemDetailsOpened()
        }
    }

}