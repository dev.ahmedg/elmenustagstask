package com.elmenus.tags


import android.os.Bundle
import androidx.test.espresso.IdlingPolicies
import androidx.test.runner.AndroidJUnitRunner
import com.elmenus.data.constant.DEFAULT_NETWORK_TIMEOUT
import com.elmenus.data.network.entities.Item
import io.appflate.restmock.RESTMockServerStarter
import io.appflate.restmock.android.AndroidAssetsFileParser
import io.appflate.restmock.android.AndroidLogger
import java.util.concurrent.TimeUnit


/**
 * Custom tet runner for the app
 *
 * Initialize RestMock library
 *
 * Must be in root package because of DexOpener (Open final kotlin classes for mocking)
 */
class CustomTestRunner : AndroidJUnitRunner() {


    override fun onCreate(arguments: Bundle) {
        super.onCreate(arguments)

        IdlingPolicies.setIdlingResourceTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)

        RESTMockServerStarter.startSync(AndroidAssetsFileParser(context), AndroidLogger())

    }



}