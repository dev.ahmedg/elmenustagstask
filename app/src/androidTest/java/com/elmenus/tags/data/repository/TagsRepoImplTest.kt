package com.elmenus.tags.data.repository

import android.content.Context
import androidx.paging.*
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.elmenus.base.utils.NetworkHelper
import com.elmenus.base.utils.jsonAssetFileToObj
import com.elmenus.data.constant.DEFAULT_PAGE_INDEX
import com.elmenus.data.constant.DEFAULT_PAGE_SIZE
import com.elmenus.data.data.pagedSource.TagsMediator
import com.elmenus.data.data.repository.tags.TagsLocalDataSource
import com.elmenus.data.data.repository.tags.TagsLocalDataSourceImpl
import com.elmenus.data.data.repository.tags.TagsRemoteDataSource
import com.elmenus.data.database.AppDatabase
import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.network.entities.TagsListResponse
import com.elmenus.tags.base.BaseTestRobot
import kotlinx.coroutines.runBlocking
import okio.IOException
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Instrumentation test class for the TagMediator cases
 *
 * Instrumentation test instead of unit test in this case because RoomDb requires android context to initialize
 *
 * Test success and error cases of the mediator using fake dependencies with mocked api data.
 */
@ExperimentalPagingApi
@RunWith(AndroidJUnit4::class)
class TagsRepoImplTest : BaseTestRobot() {

    private lateinit var remoteMediator: TagsMediator
    private lateinit var appDatabase: AppDatabase
    private lateinit var localDataSource: TagsLocalDataSource


    @Mock
    private lateinit var remoteDataSource: TagsRemoteDataSource
    @Mock
    private lateinit var networkHelper: NetworkHelper


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val context = ApplicationProvider.getApplicationContext<Context>()
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        localDataSource = TagsLocalDataSourceImpl(appDatabase)

        remoteMediator = TagsMediator(context,networkHelper,localDataSource,remoteDataSource)
    }




    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runBlocking {

        val mockedPageList = jsonAssetFileToObj<TagsListResponse>(getTestContext(),"tag_list_page.json")?.tags!!

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        mockTagsPage(mockedPageList)

        val pagingState = PagingState<Int, TagItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Success )
        assertFalse( (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached  )
    }


    @Test
    fun refreshLoadSuccessAndEndOfPaginationWhenNoMoreData() = runBlocking {

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)


        mockTagsPage(emptyList())

        val pagingState = PagingState<Int, TagItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Success )
        assertTrue( (result as RemoteMediator.MediatorResult.Success).endOfPaginationReached  )
    }


    @Test
    fun refreshLoadReturnsNetworkErrorResultWhenInternetIsNotAvailableOccurs() = runBlocking {

        val error = "No internet connection"

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        val pagingState = PagingState<Int, TagItem>(
            listOf(),
            null,
            PagingConfig(DEFAULT_PAGE_SIZE),
            DEFAULT_PAGE_SIZE
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Error )
        assertTrue( (result as RemoteMediator.MediatorResult.Error).throwable.message == error  )
    }


    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() = runBlocking {

        val exception = Exception()

        Mockito.`when`(remoteDataSource.getTags(DEFAULT_PAGE_INDEX))
            .thenThrow(exception)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)

        val pagingState = PagingState<Int, TagItem>(
            listOf(),
            null,
            PagingConfig(8),
            8
        )
        val result = remoteMediator.load(LoadType.REFRESH, pagingState)

        assertTrue( result is RemoteMediator.MediatorResult.Error  && result.throwable == exception  )
    }

    private suspend fun mockTagsPage(data: List<TagItem>) {
        Mockito.`when`(remoteDataSource.getTags(DEFAULT_PAGE_INDEX))
            .thenReturn(data)
    }


    @After
    @Throws(IOException::class)
    fun closeDb() {
        appDatabase.clearAllTables()

    }
}