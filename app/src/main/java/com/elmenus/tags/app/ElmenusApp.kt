package com.elmenus.tags.app

import androidx.multidex.MultiDexApplication
import com.elmenus.base.di.utilsModule
import com.elmenus.data.di.databaseModule
import com.elmenus.data.di.networkModule
import com.elmenus.data.di.repositoryModule
import com.elmenus.tags.di.allFeatures
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Custom app class to initialize koin dependencies graph
 */
class ElmenusApp : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@ElmenusApp)
            val modules = ArrayList(allFeatures)
            modules.addAll(listOf(databaseModule, utilsModule ,networkModule, repositoryModule))
            modules(
                modules
            )
        }

    }

}