package com.elmenus.tags.ui

import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.elmenus.base.baseEntities.BaseActivity
import com.elmenus.tags.R
import com.elmenus.tags.databinding.ActivityMainBinding

/**
 * Host activity for navigation component
 */
class MainActivity : BaseActivity<ActivityMainBinding>() {


    override fun getLayoutRes(): Int = R.layout.activity_main



    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

}