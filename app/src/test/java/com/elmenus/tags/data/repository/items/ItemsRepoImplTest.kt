package com.elmenus.tags.data.repository.items

import android.content.Context
import com.elmenus.base.utils.NetworkHelper
import com.elmenus.data.data.repository.items.ItemsLocalDataSource
import com.elmenus.data.data.repository.items.ItemsRemoteDataSource
import com.elmenus.data.data.repository.items.ItemsRepoImpl
import com.elmenus.data.network.entities.Item
import com.elmenus.tags.R
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


/**
 * Unit test class Items repository cases
 */
@RunWith(JUnit4::class)
class ItemsRepoImplTest{


    private lateinit var itemsRepo: ItemsRepoImpl

    @Mock
    private lateinit var localDataSource: ItemsLocalDataSource
    @Mock
    private lateinit var remoteDataSource: ItemsRemoteDataSource
    @Mock
    private lateinit var networkHelper: NetworkHelper
    @Mock
    private lateinit var context: Context

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        itemsRepo = ItemsRepoImpl(
            context,
            networkHelper,
            localDataSource,
            remoteDataSource
        )
    }

    /**
     * 1 - Mock remote and local data
     * 2 - Call repo getData
     * 3 - Check remote and local getData func called once
     * 4 - Check local save data called once
     * 5 - Check repo getData emitted mocked value
     * */
    @Test
    fun test_local_remote_interaction(): Unit = runBlocking {
        val tag = "1 - Egyptian"

        val mockedData = listOf(
            Item("","","",0)
        )

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(true)
        Mockito.`when`(remoteDataSource.getItemsByTag(tag))
            .thenReturn(mockedData)
        Mockito.`when`(localDataSource.getItemsByTag(tag))
            .thenReturn(mockedData)

        val allEmits = itemsRepo.getItems(tag).toList()

        assertEquals(allEmits.size,2)

        Mockito.verify(localDataSource, Mockito.times(1))
            .getItemsByTag(tag)

        Mockito.verify(localDataSource, Mockito.times(1))
            .saveItemsWithTag(tag,mockedData)

        Mockito.verify(remoteDataSource, Mockito.times(1))
            .getItemsByTag(tag)
    }


    @Test
    fun noInternetConnectionExceptionShouldBeThrownWhenNotConnected(): Unit = runBlocking {
        val tag = "1 - Egyptian"

        val error = "No internet connection"

        Mockito.`when`(context.getString(R.string.no_internet_connection))
            .thenReturn(error)

        Mockito.`when`(networkHelper.isConnected())
            .thenReturn(false)

        Mockito.`when`(localDataSource.getItemsByTag(tag))
            .thenReturn(null)

        itemsRepo.getItems(tag).catch {
            it.message shouldBeEqualTo error

            Mockito.verify(localDataSource, Mockito.times(1))
                .getItemsByTag(tag)

            Mockito.verifyNoInteractions(remoteDataSource)

        }

    }

}