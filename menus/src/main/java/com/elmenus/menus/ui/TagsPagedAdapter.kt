package com.elmenus.menus.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.elmenus.base.baseEntities.BasePagedListAdapter
import com.elmenus.data.database.entity.TagItem
import com.elmenus.menus.databinding.ItemTagBinding

class TagsPagedAdapter(private val onClick:(TagItem)->Unit) : BasePagedListAdapter() {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemTagBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position) as? TagItem ?:return
        (binding as ItemTagBinding).apply {
            model = item
            onClick = View.OnClickListener {
                onClick(item)
            }
        }
    }
}