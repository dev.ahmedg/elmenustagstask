package com.elmenus.menus.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.elmenus.base.baseEntities.BaseAdapter
import com.elmenus.data.network.entities.Item
import com.elmenus.menus.databinding.ItemItemBinding

class ItemsAdapter(private val onClick:(Int,ImageView,Item)->Unit) : BaseAdapter<Item>(object :
    DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item) = oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: Item, newItem: Item) = oldItem == newItem
}) {


    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return ItemItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position)

        (binding as ItemItemBinding).apply {
            model = item
            ivImage.transitionName = item.photoUrl
            onClick = View.OnClickListener {
                onClick(position,ivImage,item)
            }
        }
    }
}