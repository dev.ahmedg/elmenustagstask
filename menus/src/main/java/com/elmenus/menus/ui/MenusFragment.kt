package com.elmenus.menus.ui

import android.os.Bundle
import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.elmenus.base.baseEntities.BaseFragment
import com.elmenus.base.baseEntities.LoaderStateAdapter
import com.elmenus.base.utils.handleInitialState
import com.elmenus.data.UiResult
import com.elmenus.data.network.entities.Item
import com.elmenus.itemdetails.ui.ItemDetailsFragment
import com.elmenus.menus.R
import com.elmenus.menus.databinding.FragmentMenusBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * Menus fragment
 *
 * Shows list of tags
 *
 * Shows list of items for each tag
 *
 * navigate to item details on item click
 */
class MenusFragment : BaseFragment<FragmentMenusBinding>() {


    private val mViewModel by viewModel<MenusViewModel>()

    private val tagsAdapter = TagsPagedAdapter{
        mViewModel.updateItems(it.tagName)
    }
    private val itemsAdapter = ItemsAdapter{pos,iv,item->
        mViewModel.lastSelectedItemPosition = pos
        openDetailsScreen(iv,item)
    }

    private val tagsLoadStateAdapter = LoaderStateAdapter { tagsAdapter.retry() }

    private fun openDetailsScreen(iv: ImageView, item: Item) {
        val extras = FragmentNavigatorExtras(iv to item.photoUrl)
        findNavController().navigate(R.id.details_action, ItemDetailsFragment.buildArgs(item),null,extras)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExitToFullScreenTransition()
        setReturnFromFullScreenTransition()
    }

    override fun getLayoutRes() = R.layout.fragment_menus

    override fun initViewModel() {

    }



    override fun onViewAttach() {
        super.onViewAttach()
        setUpRvs()
        observeData()
    }



    private fun observeData() {
        lifecycleScope.launch {

            mViewModel.tags.distinctUntilChanged().collectLatest {
                tagsAdapter.setList(it)
            }

        }
        mViewModel.itemsState.observe(viewLifecycleOwner) {
            when(it){
                is UiResult.Loading -> showLoading(mBinding.statefulItems)
                is UiResult.Empty -> showEmpty(mBinding.statefulItems)
                is UiResult.Error ->  showError(mBinding.statefulItems,it.exception.message){
                    mViewModel.retryItemsFetch()
                }
                is UiResult.Success ->  {
                    showContent(mBinding.statefulItems)
                    itemsAdapter.submitList(it.data)

                    mViewModel.lastSelectedItemPosition?.let {
                        mBinding.rvItems.post {
                            val y = mBinding.rvItems.y + mBinding.rvItems.getChildAt(it).y
                            mBinding.scrollView.smoothScrollTo(0, y.toInt())
                            mViewModel.lastSelectedItemPosition = null
                        }
                    }

                }
            }
        }

        tagsAdapter.handleInitialState(
            onLoading = {showLoading(mBinding.statefulTags)},
            onEmpty = {showEmpty(mBinding.statefulTags)},
            onError = {showError(mBinding.statefulTags,it.message){tagsAdapter.retry()}},
            resetStates = {showContent(mBinding.statefulTags)}
        )

    }

    private fun setUpRvs() {
        mBinding.rvTags.adapter = tagsAdapter.withLoadStateFooter(tagsLoadStateAdapter)


        mBinding.rvItems.run {
            adapter = itemsAdapter
            setHasFixedSize(true)

            postponeEnterTransition()

            viewTreeObserver.addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }

        }
    }


    private fun setExitToFullScreenTransition() {
        exitTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.list_exit_transition)
    }

    private fun setReturnFromFullScreenTransition() {
        reenterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.list_return_transition)
    }


}