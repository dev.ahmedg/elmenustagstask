package com.elmenus.menus.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elmenus.data.UiResult
import com.elmenus.data.network.entities.Item
import com.elmenus.menus.domain.GetItemsByTagUseCase
import com.elmenus.menus.domain.GetTagsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * Data holder for menus screen
 *
 * Live data for lifecycle aware observer pattern
 *
 * lastSelectedItemPosition to scroll to  the same item position in the list for shared element effect
 *@param getItemsByTagUseCase Use case of tags paginated list
 *@param getItemsByTagUseCase Use case of items by tag list
 * @see MenusFragment
 */
class MenusViewModel(getTagsUseCase: GetTagsUseCase ,private val getItemsByTagUseCase: GetItemsByTagUseCase):ViewModel() {

    var lastSelectedItemPosition : Int? = null

    var lastSelectedTag :String? = null

    private val _itemsState = MutableLiveData<UiResult<List<Item>>>()

    val itemsState: LiveData<UiResult<List<Item>>> = _itemsState


    val tags = getTagsUseCase()

    fun updateItems(tagName:String) {
        lastSelectedTag= tagName

        _itemsState.value = UiResult.Loading
        viewModelScope.launch {
            getItemsByTagUseCase(tagName).catch {
                _itemsState.value = UiResult.Error(it)
            }.collect {
                _itemsState.value = if (it.isEmpty()) UiResult.Empty else UiResult.Success(it)
            }
        }
    }

    fun retryItemsFetch() {
        updateItems(lastSelectedTag!!)
    }


}