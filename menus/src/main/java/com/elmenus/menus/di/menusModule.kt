package com.elmenus.menus.di


import com.elmenus.menus.domain.GetItemsByTagUseCase
import com.elmenus.menus.domain.GetTagsUseCase
import com.elmenus.menus.ui.MenusViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Koin module for the menus screen (view model and use cases)
 */
val menusModule = module {

    factory { GetTagsUseCase(get())}

    factory { GetItemsByTagUseCase(get()) }

    viewModel { MenusViewModel(get(),get())}
}