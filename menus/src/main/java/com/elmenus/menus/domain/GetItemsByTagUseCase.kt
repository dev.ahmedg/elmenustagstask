package com.elmenus.menus.domain

import com.elmenus.data.data.repository.items.ItemsRepo


class GetItemsByTagUseCase constructor(private val repository: ItemsRepo) {

    suspend operator fun invoke(name:String) = repository.getItems(name)

}