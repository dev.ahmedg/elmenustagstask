package com.elmenus.menus.domain

import com.elmenus.data.data.repository.tags.TagsRepo


class GetTagsUseCase constructor(private val repository: TagsRepo) {

    operator fun invoke() = repository.getTags()

}