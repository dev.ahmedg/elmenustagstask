package com.elmenus.data.di


import com.elmenus.data.BuildConfig
import com.elmenus.data.constant.BASE_API_URL
import com.elmenus.data.constant.DEFAULT_NETWORK_TIMEOUT
import com.elmenus.data.network.RetrofitExecutor
import com.elmenus.data.network.api.ItemsApiService
import com.elmenus.data.network.api.TagsApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Koin module for okHttp , retrofit and it`s api interfaces
 */
val networkModule = module(override = true) {

    fun provideOkHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE }


    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }


    fun provideRetrofit(baseUrl:String, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }

    single(named("baseUrl")) { BASE_API_URL }

    single { provideOkHttpLoggingInterceptor() }

    single { provideOkHttpClient(get()) }

    single { provideRetrofit(get(named("baseUrl")),get()) }

    factory { RetrofitExecutor() }

    factory { get<Retrofit>().create(TagsApiService::class.java) }
    factory { get<Retrofit>().create(ItemsApiService::class.java) }

}