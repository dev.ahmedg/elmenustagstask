package com.elmenus.data.di


import com.elmenus.data.data.pagedSource.TagsMediator
import com.elmenus.data.data.repository.items.*
import com.elmenus.data.data.repository.tags.*
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


/**
 * Koin module for repositories , remote/local data sources an other sources
 */
val  repositoryModule = module {

    factory<ItemsLocalDataSource>{ ItemLocalDataSourceImpl(get()) }

    factory<ItemsRemoteDataSource>{ ItemsRemoteDataSourceImpl(get(),get()) }

    factory<TagsLocalDataSource>{ TagsLocalDataSourceImpl(get()) }

    factory<TagsRemoteDataSource>{ TagsRemoteDataSourceImpl(get(),get()) }

    factory{ TagsMediator(androidApplication(),get(),get(),get()) }

    factory<TagsRepo> { TagsRepoImpl(get() , get())}

    factory<ItemsRepo> { ItemsRepoImpl(androidApplication(), get(), get(), get()) }


}