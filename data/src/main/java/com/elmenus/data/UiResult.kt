package com.elmenus.data

/**
 * State management class for ui states
 * @param T result type
 */
sealed class UiResult<out T : Any> {
    data class Success<out T : Any>(val data: T?) : UiResult<T>()
    data class Error(val exception: Throwable) : UiResult<Nothing>()
    object Loading : UiResult<Nothing>()
    object Empty : UiResult<Nothing>()
}