package com.elmenus.data.data.repository.tags

import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.database.entity.TagRemoteKey
import kotlinx.coroutines.flow.Flow

interface TagsLocalDataSource {

    fun getAllTags() : PagingSource<Int, TagItem>

    suspend fun saveData(isRefresh: Boolean, tags: List<TagItem>, remoteKeys: List<TagRemoteKey>)

    suspend fun removeAlLTags()

    suspend fun getRemoteKeyByTag(tag:String) : TagRemoteKey?
}

interface TagsRemoteDataSource {

    @Throws(Exception::class)
    suspend fun getTags(page: Int): List<TagItem>

}

interface TagsRepo {

    fun getTags(): Flow<PagingData<TagItem>>

}