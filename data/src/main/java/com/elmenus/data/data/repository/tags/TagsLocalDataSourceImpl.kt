package com.elmenus.data.data.repository.tags

import androidx.paging.PagingSource
import androidx.room.withTransaction
import com.elmenus.data.database.AppDatabase
import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.database.entity.TagRemoteKey

class TagsLocalDataSourceImpl(private val appDatabase: AppDatabase) : TagsLocalDataSource {


    override fun getAllTags(): PagingSource<Int, TagItem> {
        return appDatabase.tagsDao().getAll()
    }

    override suspend fun saveData(
        isRefresh: Boolean,
        tags: List<TagItem>,
        remoteKeys: List<TagRemoteKey>
    ) {
        appDatabase.withTransaction {
            if (isRefresh){
                appDatabase.tagsDao().clearAll()
                appDatabase.tagRemoteKeyDao().clearRemoteKeys()
            }
            appDatabase.tagsDao().insertAll(tags)
            appDatabase.tagRemoteKeyDao().insertAll(remoteKeys)

        }
    }

    override suspend fun removeAlLTags() {
        appDatabase.tagsDao().clearAll()
    }

    override suspend fun getRemoteKeyByTag(tag: String): TagRemoteKey? {
        return appDatabase.tagRemoteKeyDao().remoteKeyByTagName(tag)
    }


}