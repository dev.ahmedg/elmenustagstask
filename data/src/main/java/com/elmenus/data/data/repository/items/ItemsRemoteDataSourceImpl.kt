package com.elmenus.data.data.repository.items

import com.elmenus.data.network.RetrofitExecutor
import com.elmenus.data.network.api.ItemsApiService
import com.elmenus.data.network.entities.Item

class ItemsRemoteDataSourceImpl(private val retrofitExecutor: RetrofitExecutor,private val apiService: ItemsApiService) : ItemsRemoteDataSource {

    override suspend fun getItemsByTag(tag: String): List<Item> {
        val response = retrofitExecutor.makeRequest { apiService.getItemsByTag(tag) }
        return response.items
    }
}