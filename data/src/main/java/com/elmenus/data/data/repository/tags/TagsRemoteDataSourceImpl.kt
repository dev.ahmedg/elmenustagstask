package com.elmenus.data.data.repository.tags

import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.network.RetrofitExecutor
import com.elmenus.data.network.api.TagsApiService

class TagsRemoteDataSourceImpl(private val retrofitExecutor: RetrofitExecutor, private val apiService: TagsApiService) : TagsRemoteDataSource {


    override suspend fun getTags(page: Int): List<TagItem> {
        val response = retrofitExecutor.makeRequest { apiService.getTags(page) }
        return response.tags
    }


}