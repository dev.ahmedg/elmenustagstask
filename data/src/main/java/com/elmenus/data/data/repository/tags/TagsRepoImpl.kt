package com.elmenus.data.data.repository.tags

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingData
import com.elmenus.data.constant.getDefaultPageConfig
import com.elmenus.data.data.pagedSource.TagsMediator
import com.elmenus.data.database.entity.TagItem
import kotlinx.coroutines.flow.Flow

@OptIn(ExperimentalPagingApi::class)
class TagsRepoImpl(
    private val mediator: TagsMediator,
    private val localDataSource: TagsLocalDataSource
    ) :TagsRepo {


    override fun getTags(): Flow<PagingData<TagItem>> {
        val pagingSourceFactory = { localDataSource.getAllTags()}
        return Pager(
            config = getDefaultPageConfig(),
            pagingSourceFactory = pagingSourceFactory,
            remoteMediator = mediator
        ).flow
    }


}