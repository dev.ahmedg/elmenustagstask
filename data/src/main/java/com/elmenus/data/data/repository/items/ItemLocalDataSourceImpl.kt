package com.elmenus.data.data.repository.items

import com.elmenus.base.utils.FileManager
import com.elmenus.base.utils.fromJsonArray
import com.elmenus.base.utils.toJsonArray
import com.elmenus.data.network.entities.Item

class ItemLocalDataSourceImpl(private val fileManager: FileManager) : ItemsLocalDataSource {


    override suspend fun getItemsByTag(tag: String): List<Item>? {
        val string = fileManager.readFile(getFileName(tag))
        return fromJsonArray(string)
    }

    override suspend fun saveItemsWithTag(tag: String, items: List<Item>) {
        val string = toJsonArray(items)!!
        fileManager.writeFile(string,getFileName(tag))
    }

    private fun getFileName(tag: String) = "items_by_tag_$tag"
}