package com.elmenus.data.data.repository.items

import android.content.Context
import com.elmenus.base.utils.NetworkHelper
import com.elmenus.data.data.RemoteLocalBoundResource
import com.elmenus.data.network.entities.Item
import kotlinx.coroutines.flow.Flow

class ItemsRepoImpl(private val context:Context, private val networkHelper: NetworkHelper,
                    private val localDataSource: ItemsLocalDataSource,
                    private val remoteDataSource: ItemsRemoteDataSource) : ItemsRepo {



    override suspend fun getItems(tag: String): Flow<List<Item>> {
        return RemoteLocalBoundResource(
            context,
            networkHelper,
            remoteCall = {remoteDataSource.getItemsByTag(tag)},
            localCall = {localDataSource.getItemsByTag(tag)},
            saveLocal = {localDataSource.saveItemsWithTag(tag,it)}
        ).asFlow()
    }


}