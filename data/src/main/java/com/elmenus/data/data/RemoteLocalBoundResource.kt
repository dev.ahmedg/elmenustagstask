package com.elmenus.data.data

import android.content.Context
import androidx.annotation.MainThread
import com.elmenus.base.utils.NetworkHelper
import com.elmenus.data.R
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Manage remote/local data sources caching strategy
 */
class RemoteLocalBoundResource<ResultType : Any>
@MainThread constructor(
    private val context:Context,
    private var networkHelper: NetworkHelper,
    private val remoteCall: suspend () -> ResultType,
    private val localCall: suspend () -> ResultType?,
    private val saveLocal: suspend (ResultType) -> Unit,

    ) {

    private suspend fun fetchFromNetwork(): ResultType {

        try {
            val response = remoteCall()

            saveLocal(response)

            return response

        }catch (e:Exception){
            throw e
        }

    }


    suspend fun asFlow(): Flow<ResultType> {
        return flow {
            val loadDataFromDB = localCall()
            val isLocalDataAvailable = (loadDataFromDB!=null)
            if (isLocalDataAvailable) emit(loadDataFromDB!!)

            if (networkHelper.isConnected()) {
                emit(fetchFromNetwork())
            }else{
                if (!isLocalDataAvailable) throw Exception(context.getString(R.string.no_internet_connection))
            }
        }
    }



}