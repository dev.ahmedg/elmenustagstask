package com.elmenus.data.data.pagedSource

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.elmenus.base.utils.NetworkHelper
import com.elmenus.data.R
import com.elmenus.data.constant.DEFAULT_PAGE_INDEX
import com.elmenus.data.data.repository.tags.TagsLocalDataSource
import com.elmenus.data.data.repository.tags.TagsRemoteDataSource
import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.database.entity.TagRemoteKey
import com.elmenus.data.network.entities.Result


/**
 * Mediator for caching tags paginated list from api
 */
@OptIn(ExperimentalPagingApi::class)
class TagsMediator(private val context: Context, private val networkHelper: NetworkHelper,
                   private val localDataSource: TagsLocalDataSource , private val remoteDataSource: TagsRemoteDataSource) : RemoteMediator<Int, TagItem>() {


    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, TagItem>
    ): MediatorResult {

        val page = when (val pageKeyData = getKeyPageData(loadType, state)) {
            is MediatorResult.Success -> {
                return pageKeyData
            }
            else -> {
                pageKeyData as Int
            }
        }

        if (!networkHelper.isConnected())
            return MediatorResult.Error(Exception(context.getString(R.string.no_internet_connection)))

        try {

            val response = remoteDataSource.getTags(page)
            val isEndOfList = response.isEmpty()

            val prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1
            val nextKey = if (isEndOfList) null else page + 1

            val keys = response.map {
                TagRemoteKey(tagName = it.tagName, prevKey = prevKey, nextKey = nextKey)
            }

            localDataSource.saveData(loadType == LoadType.REFRESH,response,keys)

            return MediatorResult.Success(endOfPaginationReached = isEndOfList)

        }catch (e:Exception){
            return MediatorResult.Error(e)
        }
    }

    /**
     * this returns the page key or the final end of list success result
     */
    private suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, TagItem>): Any? {

        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getClosestRemoteKey(state)
                remoteKeys?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)

                // If remoteKeys is null, that means the refresh result is not in the database yet.
                // We can return Success with endOfPaginationReached = false because Paging
                // will call this method again if RemoteKeys becomes non-null.
                // If remoteKeys is NOT NULL but its nextKey is null, that means we've reached
                // the end of pagination for append.

                return remoteKeys?.nextKey
                    ?: MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)

                // If remoteKeys is null, that means the refresh result is not in the database yet.
               return remoteKeys?.prevKey
                    ?: //end of list condition reached
                     MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
        }
    }

    /**
     * get the last remote key inserted which had the data
     */
    private suspend fun getLastRemoteKey(state: PagingState<Int, TagItem>): TagRemoteKey? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { item -> localDataSource.getRemoteKeyByTag(item.tagName)}
    }

    /**
     * get the first remote key inserted which had the data
     */
    private suspend fun getFirstRemoteKey(state: PagingState<Int, TagItem>): TagRemoteKey? {
        return state.pages
            .firstOrNull() { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { item -> localDataSource.getRemoteKeyByTag(item.tagName)}
    }

    /**
     * get the closest remote key inserted which had the data
     */
    private suspend fun getClosestRemoteKey(state: PagingState<Int, TagItem>): TagRemoteKey? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.tagName?.let { id ->
                localDataSource.getRemoteKeyByTag(id)
            }
        }
    }


}