package com.elmenus.data.data.repository.items

import com.elmenus.data.network.entities.Item
import kotlinx.coroutines.flow.Flow

interface ItemsRepo {

    suspend fun getItems(tag: String): Flow<List<Item>>

}

interface ItemsLocalDataSource {

    suspend fun getItemsByTag(tag: String) : List<Item>?

    suspend fun saveItemsWithTag(tag: String , items:List<Item>)
}

interface ItemsRemoteDataSource {

    suspend fun getItemsByTag(tag: String) : List<Item>

}