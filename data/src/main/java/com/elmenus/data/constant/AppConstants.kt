package com.elmenus.data.constant

import androidx.paging.PagingConfig
import com.elmenus.data.BuildConfig


var BASE_API_URL = BuildConfig.BASE_API_URL
const val DEFAULT_NETWORK_TIMEOUT = 100L

const val DEFAULT_PAGE_INDEX = 1
const val DEFAULT_PAGE_SIZE = 8


const val TAGS_LIST_API = "tags"
const val ITEMS_LIST_API = "items"

fun getDefaultPageConfig(): PagingConfig {
    return PagingConfig( enablePlaceholders = false,pageSize = DEFAULT_PAGE_SIZE)
}