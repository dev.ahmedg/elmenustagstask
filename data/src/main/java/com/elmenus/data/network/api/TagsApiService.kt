package com.elmenus.data.network.api


import com.elmenus.data.constant.TAGS_LIST_API
import com.elmenus.data.network.entities.TagsListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TagsApiService {

    @GET("$TAGS_LIST_API/{page}")
    suspend fun getTags(@Path("page") page: Int): Response<TagsListResponse>



}