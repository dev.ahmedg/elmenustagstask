package com.elmenus.data.network

import retrofit2.Response

/**
 * Network operation executor for retrofit library
 */
open class RetrofitExecutor {


    suspend fun <T : Any> makeRequest(
        call: suspend () -> Response<T>
    ): T {
        return safeApiResult(call)
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>
    ): T = try {
        val result = call()
        when (result.isSuccessful) {
            false -> throw Exception("Error Occurred during getting safe Api result")
            else -> result.body()!!
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        throw ex
    }
}