package com.elmenus.data.network.entities

import com.elmenus.data.database.entity.TagItem
import com.google.gson.annotations.SerializedName

data class TagsListResponse(

	@field:SerializedName("tags")
	val tags: List<TagItem>
)

