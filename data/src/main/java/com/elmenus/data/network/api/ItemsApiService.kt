package com.elmenus.data.network.api


import com.elmenus.data.constant.ITEMS_LIST_API
import com.elmenus.data.constant.TAGS_LIST_API
import com.elmenus.data.network.entities.ItemsListResponse
import com.elmenus.data.network.entities.TagsListResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ItemsApiService {


    @GET("$ITEMS_LIST_API/{tag}")
    suspend fun getItemsByTag(@Path("tag") tag: String): Response<ItemsListResponse>

}