package com.elmenus.data.database.entity

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TagRemoteKeysDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(authorRemoteKey: List<TagRemoteKey>)

    @Query("SELECT * FROM tag_remote_key WHERE tagName = :name")
    suspend fun remoteKeyByTagName(name: String): TagRemoteKey?

    @Query("DELETE FROM tag_remote_key")
    suspend fun clearRemoteKeys()
}

