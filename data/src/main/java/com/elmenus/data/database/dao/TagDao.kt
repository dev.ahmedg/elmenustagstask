package com.elmenus.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.elmenus.data.database.entity.TagItem

@Dao
interface TagDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(models: List<TagItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(model: TagItem)

    @Query("SELECT * FROM tag")
    fun getAll(): PagingSource<Int, TagItem>

    @Query("DELETE FROM tag")
    suspend fun clearAll()



}