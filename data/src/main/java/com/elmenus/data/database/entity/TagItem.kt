package com.elmenus.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.elmenus.base.baseEntities.BaseEntity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tag")
data class TagItem(

	@field:SerializedName("photoURL")
	val photoURL: String,

	@PrimaryKey @field:SerializedName("tagName")
	val tagName: String
):BaseEntity{
	override fun entityId() = tagName
}
