package com.elmenus.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Tag remote key entity for pagination operation
 * It works by storing prev and next page for each tag item to be able to append/prepend list from the item index easily
 * @see TagRemoteKeysDao
 */
@Entity(tableName = "tag_remote_key")
data class TagRemoteKey(@PrimaryKey val tagName: String, val prevKey: Int?, val nextKey: Int?)
