package com.elmenus.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.elmenus.data.database.dao.TagDao
import com.elmenus.data.database.entity.TagItem
import com.elmenus.data.database.entity.TagRemoteKey
import com.elmenus.data.database.entity.TagRemoteKeysDao

/**
 * Room database class
 */
@Database(
    entities = [TagItem::class,TagRemoteKey::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun tagsDao(): TagDao
    abstract fun tagRemoteKeyDao(): TagRemoteKeysDao

}
